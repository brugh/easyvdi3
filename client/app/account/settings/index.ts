'use strict';
import SettingsController from './settings.controller';

export default angular.module('easyvdi3App.settings', [])
  .controller('SettingsController', SettingsController)
  .name;
