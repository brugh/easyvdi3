'use strict';
import LoginController from './login.controller';

export default angular.module('easyvdi3App.login', [])
  .controller('LoginController', LoginController)
  .name;
