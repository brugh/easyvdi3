'use strict';

import SignupController from './signup.controller';

export default angular.module('easyvdi3App.signup', [])
    .controller('SignupController', SignupController)
    .name;
