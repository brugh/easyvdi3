'use strict';
import {UtilService} from './util.service';

export default angular.module('easyvdi3App.util', [])
  .factory('Util', UtilService)
  .name;
